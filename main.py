import data
import statistics
import matplotlib.pyplot as plt
import numpy as np
import np_helper
from functools import partial
from simulators import simulate_full_auto, simulate_head_shot, simulate_single_shot


def weapon_set(func, weapon, ap, skill_level, enemy_type, band):
    band = int(band)
    enemy_type = int(enemy_type)
    results = []
    for enemy in [x for x in data.ENEMIES if x[0] == data.ENEMY_TYPES[enemy_type]]:
        for _ in range(1000):
            results.append(func(weapon, ap, skill_level, band, enemy))

    val = statistics.median(results)
    return val if val <= data.MAX_RENDER_ROUNDS else None


def add_weapon_to_plot(func, ax, weapon, ap, color):
    print(weapon[0])
    X, Y, Z = np_helper.npmap2d(
        partial(weapon_set, func, weapon, ap, data.SKILL_LEVEL),
        np.linspace(0, 18, 19),
        np.linspace(0, 5, 6)
    )
    ax.plot_wireframe(X, Y, Z, color=color)


def plot_main():
    fig = plt.figure()
    ax = plt.axes(projection='3d')

    # add_weapon_to_plot(simulate_head_shot, ax, data.WEAPONS[3], 1, 'blue')
    add_weapon_to_plot(simulate_full_auto, ax, data.WEAPONS[2], 1, 'blue')
    # add_weapon_to_plot(simulate_single_shot, ax, data.WEAPONS[3], 1, 'blue')
    # add_weapon_to_plot(simulate_single_shot, ax, data.WEAPONS[2], 1, 'red')
    add_weapon_to_plot(simulate_single_shot, ax, data.WEAPONS[0], 1, 'black')

    ax.view_init(25, -145)
    ax.set_xlabel('Enemy Type')
    ax.set_ylabel('Range')
    ax.set_zlabel('Rounds')

    plt.show()


def auto_fire_diff(weapon):
    print([x[0] - x[1] for x in zip(data.RANGE_INCREMENTS[weapon],
                                    data.AUTOFIRE_INCREMENTS[weapon])])


if __name__ == '__main__':
    plot_main()
