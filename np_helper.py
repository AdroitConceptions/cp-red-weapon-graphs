import numpy as np


def npmap2d(fun, xs, ys, doPrint=False):
    Z = np.empty(len(xs) * len(ys))
    i = 0
    for y in ys:
        for x in xs:
            Z[i] = fun(x, y)
            if doPrint:
                print([i, x, y, Z[i]])
            i += 1
    X, Y = np.meshgrid(xs, ys)
    Z.shape = X.shape
    return X, Y, Z
