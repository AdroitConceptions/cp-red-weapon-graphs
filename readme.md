Code used to generate results.csv to look at various weapon/ranges/etc for CP Red weapons.

Code simulates shooting a target with a given SP & HP and counts how many rounds it takes to bring the HP to zero or less.

Open Office file has data + selection tab to generate graphs comparing weapons against various SP/HP values
