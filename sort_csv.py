import csv


with open('results.csv') as in_file:
    with open('results_sorted.csv', 'w') as out_file:
        reader = csv.reader(in_file)
        writer = csv.writer(out_file)

        all_data = [x for x in reader]

        writer.writerow(all_data[0])

        all_data = all_data[1:]
        all_data.sort()

        writer.writerows(all_data)
