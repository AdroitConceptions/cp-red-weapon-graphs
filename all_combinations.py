import data
import statistics
import csv

from multiprocessing import Pool
from tqdm import tqdm

from simulators import simulate_full_auto, simulate_head_shot, simulate_single_shot, simulate_alt_full_auto

all_items = []
for weapon in data.WEAPONS:
    for skill in data.ALL_SKILLS:
        for range_band in data.ALL_RANGES:
            for ap in data.ALL_APS:
                for fire_mode in data.ALL_FIRE_MODES:
                    for enemy in data.ENEMIES:
                        if fire_mode not in ("AutoFire", "AltAutoFire") or weapon[5] > 0:
                            all_items.append(
                                [weapon, skill, range_band, ap, fire_mode, enemy])
# all_items = all_items[:10]

# for x in all_items:
#    print(x)


def process_item(a):
    results = []
    if a[4] == "HeadShot":
        func = simulate_head_shot
    elif a[4] == "AutoFire":
        func = simulate_full_auto
    elif a[4] == "AltAutoFire":
        func = simulate_alt_full_auto
    else:
        func = simulate_single_shot

    for _ in range(data.NUMBER_OF_SIMULATIONS):
        results.append(func(a[0], a[3], a[1], a[2], a[5]))

    return [a[0][0], a[1], a[2], a[3], a[4], a[5][0], statistics.mean(results), statistics.mode(results),
            statistics.median(results), min(results), max(results)]


N = len(all_items)
pbar = tqdm(total=N)
res = []


def update(x):
    res.append(x)
    pbar.update()


pool = Pool(8)
for i in all_items:
    pool.apply_async(process_item, args=(i,), callback=update)
pool.close()
pool.join()
pbar.close()

with open("results.csv", "w") as f:
    writer = csv.writer(f)
    writer.writerow(["Weapon", "Skill", "Range Band", "AP",
                     "Mode", "SP", "Mean", "Mode", "Median", "Min", "Max"])
    for x in res:
        writer.writerow(x)
