def hp_for(x):
    return max(20, 10 + 5 * int(x/2))


ENEMIES = [(f"{x}", x, hp_for(x)) for x in range(19)]

ENEMY_TYPES = [f"{x}" for x in range(19)]

RANGE_INCREMENTS = {
    "Pistol": [13, 15, 20, 25, 30, 30, 100, 100],
    "SMG": [15, 13, 15, 20, 25, 25, 30, 100],
    "Shotgun": [13, 15, 20, 25, 30, 35, 100, 100],
    "Assault Rifle": [17, 16, 15, 13, 15, 20, 25, 30],
    "Bow": [15, 13, 15, 17, 20, 22, 100, 100],
}

AUTOFIRE_INCREMENTS = {
    "SMG": [20, 17, 20, 25, 30, 100, 100, 100],
    "Assault Rifle": [22, 20, 17, 20, 25, 100, 100, 100],
}

ALT_AUTOFIRE_INCREMENTS = {
    "SMG": [18, 15, 18, 23, 28, 100, 100, 100],
    "Assault Rifle": [20, 18, 15, 18, 23, 100, 100, 100],
}

# name, class, dice, clip, rof, auto, conceal, cost
WEAPONS = [
    ("Heavy Pistol", "Pistol", 3, 8, 2, -1, 'Y', 100),
    ("Very Heavy Pistol", "Pistol", 4, 8, 1, -1, 'N', 100),
    ("SMG", "SMG", 2, 30, 1, 3, 'Y', 100),
    ("Heavy SMG", "SMG", 3, 40, 1, 3, 'Y', 100),
    ("ShotGun", "Shotgun", 5, 4, 1, -1, 'N', 500),
    ("Assault Rifle", "Assault Rifle", 5, 25, 1, 4, 'N', 500),
    ("Bow", "Bow", 4, 100, 1, -1, 'N', 100),
]

MIN_RANGE = 0
MAX_RANGE = 4

SKILL_LEVEL = 14
MAX_RENDER_ROUNDS = 30

AP = 1

ALL_RANGES = [x for x in range(8)]
ALL_APS = [0, 1, 2]
ALL_SKILLS = [x for x in range(8, 21)]
ALL_FIRE_MODES = ["Normal", "HeadShot", "AutoFire", "AltAutoFire"]
# ALL_FIRE_MODES = ["AltAutoFire"]
NUMBER_OF_SIMULATIONS = 10000
