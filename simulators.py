import random
import data

MAX_ROUNDS = 200


def skill_roll(base):
    a = random.randint(1, 10)
    b = random.randint(1, 10) if a == 10 else 0
    c = -random.randint(1, 10) if a == 0 else 0

    return base + a + b + c


def damage_roll(dice):
    roll = [random.randint(1, 6) for _ in range(dice)]
    roll.sort()

    return [sum(roll), (5 if sum(roll[-2:]) == 12 else 0)]


def simulate_single_shot(weapon, ap, skill_level, band, enemy):
    round = 0
    sp = enemy[1]
    hp = enemy[2]
    ammo = weapon[3]

    while hp > 0:
        round += 1
        for _ in range(weapon[4]):
            if ammo == 0:
                ammo = weapon[3]
                continue
            ammo -= 1
            if skill_roll(skill_level) > data.RANGE_INCREMENTS[weapon[1]][band]:
                damage = damage_roll(weapon[2])
                hp -= (max(0, damage[0] - sp) + damage[1])
                if damage[0] > sp:
                    sp -= ap
                    sp = max(sp, 0)
        if round > MAX_ROUNDS:
            break
    return round


def simulate_head_shot(weapon, ap, skill_level, band, enemy):
    round = 0
    sp = enemy[1]
    hp = enemy[2]
    ammo = weapon[3]

    while hp > 0:
        round += 1
        if ammo == 0:
            ammo = weapon[3]
            continue
        ammo -= 1
        if skill_roll(skill_level) - 8 > data.RANGE_INCREMENTS[weapon[1]][band]:
            damage = damage_roll(weapon[2])
            hp -= (max(0, damage[0] - sp) * 2 + damage[1])
            if damage[0] > sp:
                sp -= ap
                sp = max(sp, 0)
        if round > MAX_ROUNDS:
            break
    return round


def simulate_full_auto(weapon, ap, skill_level, band, enemy):
    round = 0
    sp = enemy[1]
    hp = enemy[2]
    ammo = weapon[3]

    while hp > 0:
        round += 1
        if ammo < 10:
            ammo = weapon[3]
            continue
        ammo -= 10
        to_hit = skill_roll(skill_level)
        target = data.AUTOFIRE_INCREMENTS[weapon[1]][band]
        if to_hit > target:
            damage = damage_roll(2)
            base_damage = max(
                0, (damage[0] * min(to_hit - target, weapon[5])))
            hp -= (max(0, base_damage - sp) + damage[1])
            if base_damage > sp:
                sp -= ap
                sp = max(sp, 0)
        if round > MAX_ROUNDS:
            break
    return round


def simulate_alt_full_auto(weapon, ap, skill_level, band, enemy):
    round = 0
    sp = enemy[1]
    hp = enemy[2]
    ammo = weapon[3]

    while hp > 0:
        round += 1
        if ammo < 10:
            ammo = weapon[3]
            continue
        ammo -= 10
        to_hit = skill_roll(skill_level)
        target = data.ALT_AUTOFIRE_INCREMENTS[weapon[1]][band]
        if to_hit > target:
            damage_dice = min(weapon[2], 4) + min(to_hit - target - 1, 4)

            damage = damage_roll(damage_dice)
            hp -= (max(0, damage[0] - sp) + damage[1])
            if damage[0] > sp:
                sp -= ap
                sp = max(sp, 0)
        if round > MAX_ROUNDS:
            break
    return round
